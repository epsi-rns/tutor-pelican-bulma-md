
### Tutor 03

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Pelican Bulma: Tutor 03][pelican-bulma-preview-03]

[pelican-bulma-preview-03]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-03/pelican-bulma-md-preview.png

