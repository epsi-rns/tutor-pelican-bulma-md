
### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Pelican Theme

* General Layout: Base, Page, Article, Index

* Partials: HTML Head, Header, Footer

* Jinja2: Manage Heading Title

* Basic Content: Pages, Posts

![Pelican Bulma: Tutor 01][pelican-bulma-preview-01]

[pelican-bulma-preview-01]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-01/pelican-bulma-md-preview.png

