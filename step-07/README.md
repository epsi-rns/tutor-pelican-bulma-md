
### Tutor 07

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Macros (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Python Plugin: Jinja2Content

* Multi Column Responsive List: Categories, Tags, and Archives

![Pelican Bulma: Tutor 07][pelican-bulma-preview-07]

[pelican-bulma-preview-07]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-07/pelican-bulma-md-preview.png

