# Pelican Bulma Test Drive 

An example of Pelican site using Bulma Material Design
for personal learning purpose.

> Pelican (Jinja2) + Bulma (Material Design)

![Pelican Bulma: Tutor][pelican-bulma-preview]

-- -- --

## Links

### Pelican Step By Step

This repository:

* [Pelican Step by Step Repository][tutorial-pelican]

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

### Comparison

Comparation with other static site generator

* [Jekyll Step by Step Repository][tutorial-jekyll]

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Eleventy (Bulma MD) Step by Step Repository][tutorial-11ty-b]]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-11ty-b]:      https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Pelican Theme

* General Layout: Base, Page, Article, Index

* Partials: HTML Head, Header, Footer

* Jinja2: Manage Heading Title

* Basic Content: Pages, Posts

![Pelican Bulma: Tutor 01][pelican-bulma-preview-01]

-- -- --

### Tutor 02

> Generate Complete Pure HTML Template

* Additional List Layout: Archives, Period, Authors, Categories, Tags

* Additional Single Layout: Author, Category, Tag

* Jinja2: Basic Loop

* More Content: Lyrics. Need this content for demo

![Pelican Bulma: Tutor 02][pelican-bulma-preview-02]

-- -- --

### Tutor 03

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Pelican Bulma: Tutor 03][pelican-bulma-preview-03]

-- -- --

### Tutor 04

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Index Content: Index, Categories, Tags, Periods, Archives By Year and Month

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Pelican Bulma: Tutor 04][pelican-bulma-preview-04]

-- -- --

### Tutor 05

> Loop with Jinja2

* Jinja2 Template Inheritance: Simplified All Layout

* List Tree: Archives: By Year and Month

* List Tree: Tags and Categories

* Article Index: Blog Posts

* Index Layout: Blog List, Terms, By Chronology

* Home: Landing Page

* Custom Output: Text

![Pelican Bulma: Tutor 05][pelican-bulma-preview-05]

-- -- --

### Tutor 06

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination: Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

* Python Data: Archives, Friends

* Python: Jinja2 Filter

![Pelican Bulma: Tutor 06][pelican-bulma-preview-06]

-- -- --

### Tutor 07

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Macros (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Python Plugin: Jinja2Content

* Multi Column Responsive List: Categories, Tags, and Archives

![Pelican Bulma: Tutor 07][pelican-bulma-preview-07]

-- -- --

What do you think ?

[pelican-bulma-preview]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/pelican-bulma-md-preview.png
[pelican-bulma-preview-01]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-01/pelican-bulma-md-preview.png
[pelican-bulma-preview-02]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-02/pelican-bulma-md-preview.png
[pelican-bulma-preview-03]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-03/pelican-bulma-md-preview.png
[pelican-bulma-preview-04]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-04/pelican-bulma-md-preview.png
[pelican-bulma-preview-05]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-05/pelican-bulma-md-preview.png
[pelican-bulma-preview-06]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-06/pelican-bulma-md-preview.png
[pelican-bulma-preview-07]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-07/pelican-bulma-md-preview.png
