
### Tutor 05

> Loop with Jinja2

* Jinja2 Template Inheritance: Simplified All Layout

* List Tree: Archives: By Year and Month

* List Tree: Tags and Categories

* Article Index: Blog Posts

* Index Layout: Blog List, Terms, By Chronology

* Home: Landing Page

* Custom Output: Text

![Pelican Bulma: Tutor 05][pelican-bulma-preview-05]

[pelican-bulma-preview-05]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-05/pelican-bulma-md-preview.png

