Title:    The River
Date:     2014-03-15 07:35
Category: lyric
Tags:     springsteen, 80s
Slug:     bruce-springsteen-the-river
Author:   Bruce Springsteen
RelatedLinkIDS: 14032535, 14031535

Is a dream a lie 
if it don't come true? 
Or is it something worse?
