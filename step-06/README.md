
### Tutor 06

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination: Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

* Python Data: Archives, Friends

* Python: Jinja2 Filter

![Pelican Bulma: Tutor 06][pelican-bulma-preview-06]

[pelican-bulma-preview-06]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-06/pelican-bulma-md-preview.png

