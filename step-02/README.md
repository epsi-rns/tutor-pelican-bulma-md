
### Tutor 02

> Generate Complete Pure HTML Template

* Additional List Layout: Archives, Period, Authors, Categories, Tags

* Additional Single Layout: Author, Category, Tag

* Jinja2: Basic Loop

* More Content: Lyrics. Need this content for demo

![Pelican Bulma: Tutor 02][pelican-bulma-preview-02]

[pelican-bulma-preview-02]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-02/pelican-bulma-md-preview.png

