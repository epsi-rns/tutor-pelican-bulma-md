Test URL

* <http://localhost:8000/>

* <http://localhost:8000/pages/about>

* <http://localhost:8000/2014/03/15/bruce-springsteen-the-river/>

* <http://localhost:8000/archives/>

* <http://localhost:8000/archives/2018/>

* <http://localhost:8000/archives/2014/03/>

* <http://localhost:8000/tags/>

* <http://localhost:8000/tag/indie/>

* <http://localhost:8000/categories/>

* <http://localhost:8000/category/lyric/>

* <http://localhost:8000/author/>

* <http://localhost:8000/author/bruce-springsteen/>

INDEX_URL (not set yet)

* <http://localhost:8000/blog/>
