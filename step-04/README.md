
### Tutor 04

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Index Content: Index, Categories, Tags, Periods, Archives By Year and Month

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Pelican Bulma: Tutor 04][pelican-bulma-preview-04]

[pelican-bulma-preview-04]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-04/pelican-bulma-md-preview.png

